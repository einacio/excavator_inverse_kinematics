import rclpy
from rclpy.node import Node
from geometry_msgs.msg import PoseStamped, Point
from visualization_msgs.msg import Marker

class LineMarkerPublisher(Node):
    def __init__(self):
        super().__init__('line_marker_publisher')

        self.shovel_poses=[]

        #Subscribe to the shovel_pose topic
        self.shovel_pose_subscription = self.create_subscription( PoseStamped,
                                                                  '/shovel_tip_pose',
                                                                  self.shovel_pose_callback,
                                                                  10)
        
        #create a marker publisher
        self.marker_publisher = self.create_publisher(Marker, '/shovel_tip_marker',10)

        self.timer = self.create_timer(0.1, self.publish_line_marker)

    def shovel_pose_callback(self, msg):
        #store the received shovel pose in the list
        self.shovel_poses.append(msg.pose.position)

    def publish_line_marker(self):
        if len(self.shovel_poses)<2:
            return #Not enough poses to create a line
        
        marker=Marker()
        marker.header.frame_id ="world"
        marker.header.stamp = self.get_clock().now().to_msg()
        marker.id =0
        marker.type = Marker.LINE_STRIP
        marker.scale.x = 0.02 #line width
        marker.color.a = 1.0
        marker.color.r = 0.0
        marker.color.g = 1.0
        marker.color.b = 0.0

        #addppoints to the line marker
        marker.points = [Point(x=pose.x, y=pose.y, z=pose.z) for pose in self.shovel_poses]

        #publishe the line marker
        self.marker_publisher.publish(marker)

def main (args=None):
    rclpy.init(args=args)

    shovel_pose_subscriber = LineMarkerPublisher()

    rclpy.spin(shovel_pose_subscriber)

    shovel_pose_subscriber.destroy_node()

    rclpy.shutdown()

if __name__ == '__main__':
    main()
