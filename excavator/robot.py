from rclpy.node import Node
from sensor_msgs.msg import JointState
from .inverse_kinematic_calculation2 import next_point, path_plan, slope_next_point
from math import radians, pi
import math
import numpy as np
import rclpy
from std_msgs.msg import String
import tkinter as tk


class RobotDefinition(Node):
    
    def __init__(self):

        super().__init__('main_node')

        self.joint_state_publisher = self.create_publisher(JointState, 'joint_states', 10)

        self.joint = JointState()

        self.joint.header.frame_id = 'robot'
        self.joint.name            = ['body_rotation', 'arm1_rotation', 'arm2_rotation', 'shovel_rotation']

        #initial angles for each joint
        self.body_rotation = 0.0
        self.arm1_rotation =  -0.698
        self.arm2_rotation = -1.256
        self.shovel_rotation = 1.376
        
        #Creating a subscription to get the input for the values
        self.subscription = self.create_subscription(String,
                                                     'entry',
                                                     self.get_values_callback,
                                                     10)
        #initial input if some initial numbers is missing
        self.xE=3000
        self.yE=-800
        self.slope=0
        self.emergency_stop= False #
        
        #Inverse Kinematics calculation, the IK have different references so have different angles
        self.alpha=[0, 0.7726491304185173, 1.06856539048882, 1.8585330359687762, 0] # angles for each joint arm1= alpha[1], arm2=alpha[2], shovel=alpha[3]
        self.q=[] #angle for calculation
        self.success=False #it's possible to reach the next point, starts as false because it's waiting the entry
        self.alpha2=[] #to create the motion alpha2-alpha
        self.a=[0, 932, 2586, -1362, 588] #dimensions in x for the IK
        self.b = [0, 912, -625, -154, 456] #dimensions in y for the IK
        self.q, self.success = next_point(self.xE, self.yE, self.a, self.b, self.alpha)
        self.calculation=True
        self.i=0
        self.first_time=True
        self.angle=[]
        
        
        self.joint.position = [self.body_rotation, self.arm1_rotation, self.arm2_rotation, self.shovel_rotation]

        self.timer_period = 0.1

        self.timer = self.create_timer(self.timer_period, self.timer_callback)

        self.direction = 0

        self.get_logger().info("main node is now on...")

        self.mensagem = ""
        
    def timer_callback(self):
        
        if self.emergency_stop:
            return
        
        self.joint.header.stamp = self.get_clock().now().to_msg()
        
        self.get_joints()

        self.joint.position = [np.radians(self.body_rotation), np.radians(self.arm1_rotation), np.radians(self.arm2_rotation), np.radians(self.shovel_rotation)]

        #messages to print in the prompt

        self.get_logger().info(f"Activity : {self.mensagem}")
        
        self.get_logger().info(f"Sending 'body_rotation': {self.body_rotation}")

        self.get_logger().info(f"Sending 'arm1_rotation': {self.arm1_rotation}")

        self.get_logger().info(f"Sending 'arm2_rotation': {self.arm2_rotation}")

        self.get_logger().info(f"Sending 'shovel_rotation': {self.shovel_rotation}")
        
        self.joint_state_publisher.publish(self.joint)
        
    def emergency_button_callback(self): #emergency button to stop the machine
        self.get_logger().info("Emergency Stop Button Pressed")
        self.direction = 0  # Set direction to 0 to stop movement
        self.emergency_stop = True  # Set emergency stop flag
        
    def get_values_callback(self,msg): #msg

        # Extract xe, ye, and slope values from the received message
        values = msg.data.split(',')
        xe_value = float(values[0].split(':')[1].strip())
        ye_value = float(values[1].split(':')[1].strip())
        slope_value = float(values[2].split(':')[1].strip())
            
            # Update the internal state of the robot with the received values
        self.xE = xe_value
        self.yE = ye_value
        self.slope = slope_value
            
        self.direction = 1

        self.get_logger().info(f"Received engine parameters: xe={xe_value}, ye={ye_value}, slope={slope_value}")
       
    def get_joints(self): #direction is decide what type of movement

        #emergency stop
        if self.emergency_stop == True:
            self.direction=0
            self.destroy()           
            
        #to position the machine in the opening position    
        if self.first_time == True:
            self.body_rotation = 0.0
            self.arm1_rotation =  math.degrees(-0.698)
            self.arm2_rotation = math.degrees(-1.256)
            self.shovel_rotation = math.degrees(-1.376)
            self.first_time = False

        #to stop the machine if can't reach the next point
        if self.success == False:
            self.direction = 0
            self.mensagem=("success : ",self.success, "Impossible to reach the next point")

        #direction 0 the motor is off
        if self.direction ==0:
            self.mensagem="Motor Off \n Waiting for a new entry"

        #going for the first point but in 300 height , in a constante time
        if self.direction==1:
            self.mensagem=(f"going for the point {self.xE} and -300")
            if self.calculation == True:
                self.i = 0
                self.q, self.success = next_point(self.xE, 300, self.a, self.b, self.alpha) #function next_point is for getting the angle for a forward kinematics
                self.alpha2 = path_plan(self.q,self.a,self.b,self.alpha) #to get the angles for the joint
                self.angle = (0,(self.alpha2[1]-self.alpha[1])/100,(self.alpha2[2]-self.alpha[2])/100,(self.alpha2[3]-self.alpha[3])/100) #divide by 100 to create the motion
                self.calculation = False #just to calculate once
            if self.i == 100:
                self.i = 0
                self.alpha = self.alpha2
                self.direction +=1
                self.calculation = True
            
            #getting the values to print each movement for the machine
            self.i+=1
            self.arm1_rotation -= math.degrees(self.angle[1])
            self.arm2_rotation -= math.degrees(self.angle[2])
            self.shovel_rotation -= math.degrees(self.angle[3])
        
        #going for goal point
        if self.direction==2:
            self.mensagem = (f"going for the point {self.xE} and {self.yE}")
            if self.calculation == True:
                self.q, self.success = next_point(self.xE, self.yE, self.a, self.b, self.alpha)
                self.alpha2 = path_plan(self.q,self.a,self.b,self.alpha)
                self.angle = (0,(self.alpha2[1]-self.alpha[1])/100,(self.alpha2[2]-self.alpha[2])/100,(self.alpha2[3]-self.alpha[3])/100)
                self.calculation = False

            if self.i == 100:
                self.i = 0
                self.alpha = self.alpha2
                self.direction +=1
                self.calculation = True
            
            #getting the values to print each movement for the machine
            self.i+=1
            self.arm1_rotation -= math.degrees(self.angle[1])
            self.arm2_rotation -= math.degrees(self.angle[2])
            self.shovel_rotation -= math.degrees(self.angle[3])

        #returning the shovel outside the ground
        if self.direction==3:
            self.mensagem="Removing the shovel from the ground"
            if self.calculation == True:
                self.i = 0
                self.q, self.success = next_point(self.xE, 650, self.a, self.b, self.alpha)
                self.alpha2 = path_plan(self.q,self.a,self.b,self.alpha)
                self.angle = (0,(self.alpha2[1]-self.alpha[1])/100,(self.alpha2[2]-self.alpha[2])/100,(self.alpha2[3]-self.alpha[3])/100)
                self.calculation = False

            if self.i == 100:
                self.i = 0
                self.alpha = self.alpha2
                self.direction +=1
                self.calculation = True
            
            #getting the values to print each movement for the machine

            self.i+=1
            self.arm1_rotation -= math.degrees(self.angle[1])
            self.arm2_rotation -= math.degrees(self.angle[2])
            self.shovel_rotation -= math.degrees(self.angle[3])

        #rotating 90 degrees counterclock wise  constante speed #forward kinematic
        if self.direction==4:
            self.mensagem="rotating 90 degrees counterclock wise"
            self.body_rotation += 0.5
            if self.body_rotation == 90:
                self.direction+=1
        
        #rotating droping the material #forward kinematic  
        if self.direction==5:
            self.mensagem="droping the material"
            self.i+=1
            self.arm2_rotation -=0.20
            self.shovel_rotation -= 0.5
            if self.i == 240:
                self.direction+=1
                self.i=0
                
        #returning the shovel droping the material  the movement is twice fast because it doesn't have material inside the shovel 
        if self.direction==6:
            self.mensagem="returning the shovel"
            self.arm2_rotation +=0.4
            self.shovel_rotation += 1
            self.i+=1
            if self.i==120:
                self.direction+=1
                self.i=0
                
        #returning the body to the origin the movement is twice fast
        if self.direction==7:
            self.mensagem="rotating 90 degrees clockwise"
            self.body_rotation -= 1
            if self.body_rotation == 0:
                self.direction += 1
                self.calculation = True
                #self.xE, self.yE = slope_next_point(self.slope,self.xE,self.yE)
                                
        #returning the shovel outside the ground
        if self.direction==8:
            self.mensagem="returning to the origin and calculating the next point"
            if self.calculation == True:
                self.i = 0
                self.q, self.success = next_point(3400, 300, self.a, self.b, self.alpha)
                self.alpha2 = path_plan(self.q,self.a,self.b,self.alpha)
                self.angle = (0,(self.alpha2[1]-self.alpha[1])/50,(self.alpha2[2]-self.alpha[2])/50,(self.alpha2[3]-self.alpha[3])/50)
                self.calculation = False
            if self.i == 50:
                self.i = 0
                self.alpha = self.alpha2
                self.direction = 1
                self.calculation = True
                self.xE, self.yE = slope_next_point(self.slope,self.xE,self.yE)
            
            #getting the values to print each movement for the machine
            self.i += 1
            self.arm1_rotation -= math.degrees(self.angle[1])
            self.arm2_rotation -= math.degrees(self.angle[2])
            self.shovel_rotation -= math.degrees(self.angle[3])
    
def main(args = None):

    rclpy.init(args = args)
    robot = RobotDefinition()
    
    rclpy.spin(robot)

if __name__ == '__main__':
    
    main()