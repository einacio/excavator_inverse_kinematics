import tkinter as tk
import rclpy
from std_msgs.msg import String

class EnterNumbers(tk.Frame):
    def __init__(self, master=None, num_floors=10):
        super().__init__(master)
        self.master = master
        self.create_widgets()
        self.publisher = publisher
        self.botton_publisher = self.create_publisher(String, 'engine_parameters', 10)
        self.xE=3000
        self.yE=-800
        self.slope=10

    def create_widgets(self):
        tk.Label(self, text="xE in mm = ").grid(row=0, column=0, padx=5, pady=5)
        tk.Label(self, text="yE in mm = ").grid(row=0, column=1, padx=5, pady=5)
        tk.Label(self, text="slope in degrees = ").grid(row=0, column=2, padx=5, pady=5)

        self.xe = tk.Entry(self)
        self.xe.grid(row=1, column=0, padx=5, pady=5)

        self.ye = tk.Entry(self)
        self.ye.grid(row=1, column=1, padx=5, pady=5)

        self.slope = tk.Entry(self)
        self.slope.grid(row=1, column=2, padx=5, pady=5)
        
        btn = tk.Button(self, text="Start my engines", command=self.self.publish_values)
        btn.grid(row=2, column=0, padx=10, pady=10)
        
        btn = tk.Button(self, text="Emergency Stop", command=self.emergency_stop)
        btn.grid(row=3, column=0, padx=10, pady=10)

    def emergency_stop(self):
        print("Emergency stop button pressed")
        self.master.destroy()
    
    def publish_values(self):
        xe_value = self.xe.get()
        ye_value = self.ye.get()
        slope_value = self.slope.get()

        if self.publisher:
            # Publish the values to the ROS 2 topic
            msg = String()
            msg.data = f"xe: {xe_value}, ye: {ye_value}, slope: {slope_value}"
            self.publisher.publish(msg)
            print("Published values:", msg.data)

            # Disable the button after publishing once
            self.master.destroy()

def main():
    rclpy.init()

    # Create an instance of the ROS 2 publisher
    publisher = rclpy.create_publisher(String, 'engine_parameters', 10)
    

    app = tk.Tk()
    enter_numbers = EnterNumbers(master=app, publisher=publisher)
    enter_numbers.pack()

    app.mainloop()

if __name__ == "__main__":
    main()
